#!/bin/bash
#
APIHOST="http://127.0.0.1:8000"

echo $APIHOST
echo "Create customer 1 saving account"

# create SAVING account
curl -X POST \
{$APIHOST}/api/v1/accounts \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-d '{"customer_id": 1, "type": "SAVING"}'

echo ""

echo "Create customer 1 cheque account"

# create CHEQUE account with same customer
curl -X POST \
{$APIHOST}/api/v1/accounts \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-d '{"customer_id": 1, "type": "CHEQUE"}'

echo ""

echo "Create customer 2 saving account"

# create SAVING account with other customer
curl -X POST \
{$APIHOST}/api/v1/accounts \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-d '{"customer_id": 2, "type": "SAVING"}'

echo ""

echo "Account 1 deposit"

#Deposit

curl -X POST \
{$APIHOST}/api/v1/accounts/1/deposit \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-d '{"amount":100000.10}'

echo ""

echo "Account 1 withdraw"

#Withdraw

curl -X POST \
{$APIHOST}/api/v1/accounts/1/withdraw \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-H 'Postman-Token: c293b9ba-1f0d-48ea-a3b6-361b848d6ec8' \
-d '{"amount":100.00}'

echo ""

echo "Transfer account 1 to 2"

#Transfer
# transfer to same customer, no charge
curl -X PUT \
  {$APIHOST}/api/v1/accounts/transfer \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
    "from_account_id":1,
    "to_account_id": 2,
    "amount":100.00
}'

echo ""

echo "Transfer account 1 to 3"

# transfer to same customer, charged
curl -X PUT \
  {$APIHOST}/api/v1/accounts/transfer \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
    "from_account_id":1,
    "to_account_id": 3,
    "amount":100.00
}'

echo ""

echo "Account 1 balance"

#Balance
curl -X GET \
{$APIHOST}/api/v1/accounts/1 \
-H 'Cache-Control: no-cache'

echo ""

echo "Delete account 2"

#Delete
curl -X DELETE \
{$APIHOST}/api/v1/accounts \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-d '{"account_id":2}'

echo ""

echo "Account 2 balance"

#Balance
curl -X GET \
{$APIHOST}/api/v1/accounts/2 \
-H 'Cache-Control: no-cache'

echo ""