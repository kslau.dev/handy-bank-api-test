## Handy bank-api-test

@developer ks1227@gmail.com

### Database setup

	cd ./src
	# config database connection
	vi .env 
	# start migrate
	php artisan migrate:install

### Start dev server
	cd ./src
	php artisan serve

### Create account
	# create SAVING account
	curl -X POST \
	http://127.0.0.1:8000/api/v1/accounts \
	-H 'Cache-Control: no-cache' \
	-H 'Content-Type: application/json' \
	-d '{"customer_id": 1, "type": "SAVING"}'
	
	# create CHEQUE account with same customer
	curl -X POST \
	http://127.0.0.1:8000/api/v1/accounts \
	-H 'Cache-Control: no-cache' \
	-H 'Content-Type: application/json' \
	-d '{"customer_id": 1, "type": "CHEQUE"}'
	
	# create SAVING account with other customer
	curl -X POST \
	http://127.0.0.1:8000/api/v1/accounts \
	-H 'Cache-Control: no-cache' \
	-H 'Content-Type: application/json' \
	-d '{"customer_id": 2, "type": "SAVING"}'

### Deposit
	curl -X POST \
	http://127.0.0.1:8000/api/v1/accounts/1/deposit \
	-H 'Cache-Control: no-cache' \
	-H 'Content-Type: application/json' \
	-d '{"amount":100000.10}'
	
### Withdraw
	curl -X POST \
	http://127.0.0.1:8000/api/v1/accounts/1/withdraw \
	-H 'Cache-Control: no-cache' \
	-H 'Content-Type: application/json' \
	-H 'Postman-Token: c293b9ba-1f0d-48ea-a3b6-361b848d6ec8' \
	-d '{"amount":100.00}'
	
	
## Transfer
	# transfer to same customer, no charge
	curl -X PUT \
	  http://127.0.0.1:8000/api/v1/accounts/transfer \
	  -H 'Cache-Control: no-cache' \
	  -H 'Content-Type: application/json' \
	  -d '{
		"from_account_id":1,
		"to_account_id": 2,
		"amount":100.00
	}'
	
	# transfer to same customer, charged
	curl -X PUT \
	  http://127.0.0.1:8000/api/v1/accounts/transfer \
	  -H 'Cache-Control: no-cache' \
	  -H 'Content-Type: application/json' \
	  -d '{
		"from_account_id":1,
		"to_account_id": 3,
		"amount":100.00
	}'

## Balance
	curl -X GET \
	http://127.0.0.1:8000/api/v1/accounts/1 \
	-H 'Cache-Control: no-cache'

## Delete
	curl -X DELETE \
	http://127.0.0.1:8000/api/v1/accounts \
	-H 'Cache-Control: no-cache' \
	-H 'Content-Type: application/json' \
	-d '{"account_id":2}'