<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::prefix('v1')->group(function () {
    Route::get('/', function(){
    	return response()->json([
    		"GET /accounts/{ID}",
    		"POST /accounts",
    		"POST /accounts/{ID}/deposit",
    		"POST /accounts/{ID}/withdraw",
    		"PUT /accounts",
    		"DELETE /accounts",
    	]);
    });
    Route::get('accounts', 'AccountController@index');
    Route::get('accounts/{id}', 'AccountController@show');
    Route::post('accounts', 'AccountController@store');
    Route::put('accounts/transfer', 'AccountController@transfer');
    Route::delete('accounts', 'AccountController@destroy');

    Route::post('accounts/{id}/deposit', 'AccountController@deposit');
    Route::post('accounts/{id}/withdraw', 'AccountController@withdraw');
});