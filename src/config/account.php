<?php

return [
	'transfer' => [
		'daily_quota' => 10000,
		'charge' => [
			'self' => 0,
			'other' => 100
		]
	]
];