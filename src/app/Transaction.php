<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table      = 'transactions';
    protected $primaryKey = 'transaction_id';

    public static function checkDailyQuotaExceed($account_id, $quota = 0)
    {
        $total = self::where('type', 'TRANSFER_OUT')
            ->where('created_at', '>=', Carbon::now()->startOfDay())
            ->where('created_at', '<=', Carbon::now()->endOfDay())
            ->sum('amount');
        return ($quota != 0 && $total > $quota) ? true : false;
    }
}
