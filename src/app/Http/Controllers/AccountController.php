<?php

namespace App\Http\Controllers;

use App\Account;
use App\Transaction;
use Illuminate\Http\Request;
use Log;
use Validator;

class AccountController extends Controller
{
    private $error;

    private $return;

    private $status_code = 200;

    public function __construct()
    {
        $this->error  = include_once resource_path('/error.php');
        $this->return = ['status' => 'success'];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->return, $this->status_code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::debug(__METHOD__);
        $return = ["status" => "success"];
        $code   = 200;
        $data   = $request->all();

        $rules = [
            'customer_id' => 'required',
            'type'        => 'required',
        ];

        if ($this->_validate($data, $rules)) {
            $fields         = ['customer_id', 'type', 'status'];
            $data['status'] = "AVAILABLE"; //AVAILABLE, BLOCKED, DELETED
            $account        = new Account;

            foreach ($fields as $field) {
                if (isset($data[$field])) {
                    $account->$field = $data[$field];
                }
            }

            $account->save();
            $this->return['account_id'] = $account->account_id;
        }

        return response()->json($this->return, $this->status_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        Log::debug(__METHOD__);
        // check account exist
        $account = Account::find($id);
        if (!$account) {
            $this->return      = ["status" => "fail", "error" => 4002, "message" => $this->error[4002]];
            $this->status_code = 400;
        } else {
            $this->return = ["status" => "success", "balance" => number_format($account['balance'], 2)];
        }
        return response()->json($this->return, $this->status_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Log::debug(__METHOD__);
        $return = ["status" => "success"];
        $code   = 200;
        $data   = $request->all();

        $rules = [
            'account_id' => 'required',
        ];

        if ($this->_validate($data, $rules)) {
            // check account exist
            $account = Account::find($data['account_id']);
            if (!$account) {
                $this->return      = ["status" => "fail", "error" => 4002, "message" => $this->error[4002]];
                $this->status_code = 400;
            } elseif ($account['balance'] != 0) {
                $this->return      = ["status" => "fail", "error" => 4005, "message" => $this->error[4005]];
                $this->status_code = 400;
            } else {
                // delete account
                // $account->status = "DELETED";
                $account->delete();
            }
        }
        return response()->json($this->return, $this->status_code);
    }

    /**
     * deposit
     *
     * @return \Illuminate\Http\Response
     */
    public function deposit(Request $request, $id)
    {
        Log::debug(__METHOD__);

        $data = $request->all();

        $rules = [
            'amount' => 'required|numeric',
        ];

        if ($this->_validate($data, $rules)) {
            // check account exist
            $account = Account::find($id);
            if (!$account) {
                $this->return      = ["status" => "fail", "error" => 4002, "message" => $this->error[4002]];
                $this->status_code = 400;
            } else {
                $data['balance'] = (float) $account['balance'] + (float) $data['amount'];
                // deposit
                $this->_writeTransaction("DEPOSIT", $id, $data['amount'], $data['balance']);

                $account->balance = $data['balance'];
                $account->save();
            }
        }
        return response()->json($this->return, $this->status_code);
    }

    /**
     * withdraw
     *
     * @return \Illuminate\Http\Response
     */
    public function withdraw(Request $request, $id)
    {
        Log::debug(__METHOD__);

        $data = $request->all();

        $rules = [
            'amount' => 'required|numeric',
        ];

        if ($this->_validate($data, $rules)) {
            // check account exist
            $account = Account::find($id);
            if (!$account) {
                $this->return      = ["status" => "fail", "error" => 4002, "message" => $this->error[4002]];
                $this->status_code = 400;
            } else {
                $data['balance'] = (float) $account['balance'] - (float) $data['amount'];
                if ($data['balance'] < 0) {
                    $this->return      = ["status" => "fail", "error" => 4003, "message" => $this->error[4003]];
                    $this->status_code = 400;
                } else {

                    // withdraw
                    $this->_writeTransaction("WITHDRAW", $id, $data['amount'], $data['balance']);

                    $account->balance = $data['balance'];
                    $account->save();
                }
            }
        }
        return response()->json($this->return, $this->status_code);
    }

    /**
     * transfer
     *
     * @return \Illuminate\Http\Response
     */
    public function transfer(Request $request)
    {
        Log::debug(__METHOD__);

        $data = $request->all();

        $rules = [
            'from_account_id' => 'required',
            'to_account_id'   => 'required',
            'amount'          => 'required',
        ];
        
        if ($this->_validate($data, $rules)) {
            // check account exist
            $account        = [];
            $account['out'] = Account::find($data['from_account_id']);
            $account['in']  = Account::find($data['to_account_id']);

            if (!$account['out'] || !$account['in']) {
                $this->return      = ["status" => "fail", "error" => 4002, "message" => $this->error[4002]];
                $this->status_code = 400;
            } else {

                if ($account['out']['balance'] = $this->_check_balance($account['out']['balance'], $data['amount'])) {

                    if (Transaction::checkDailyQuotaExceed($account['out']['account_id'], config('account.transfer.daily_quota'))) {
                        // check quota
                        $this->return      = ["status" => "fail", "error" => 4004, "message" => $this->error[4004]];
                        $this->status_code = 400;
                    } else {
                        $is_charge = false;
                        $charge    = config('account.transfer.charge.other', 0);
                        // check is same customer
                        if ($account['out']['customer_id'] != $account['in']['customer_id']) {
                            Log::debug("Is not same customer");
                            // check balance
                            if (!$this->_check_balance($account['out']['balance'], $charge)) {
                                return response()->json($this->return, $this->status_code);
                            }
                            $is_charge       = true;
                            $data['balance'] = $account['out']['balance'] - $charge;
                        } else {
                            $data['balance'] = $account['out']['balance'];
                        }

                        $this->_writeTransaction('TRANSFER_OUT', $account['out']['account_id'], $data['amount'], $account['out']['balance']);

                        $account['in']['balance'] = $account['in']['balance'] + $data['amount'];
                        $this->_writeTransaction('TRANSFER_IN', $account['in']['account_id'], $data['amount'], $account['in']['balance']);

                        if ($is_charge) {
                            Log::debug("Charge transction, Charge: " . $charge . ", Balance: " . $data['balance']);
                            $this->_writeTransaction('TRANSFER_CHARGE', $account['out']['account_id'], $charge, $data['balance']);
                        }

                        // save transfer out account balance
                        $account['out']->balance = $data['balance'];
                        $account['out']->save();

                        // save transfer in account balance
                        $account['in']->save();
                    }
                }
            }
        }
        return response()->json($this->return, $this->status_code);
    }

    private function _check_balance($balance, $amount)
    {
        $new_balance = (float) $balance - (float) $amount;
        if ($new_balance < 0) {
            $this->return      = ["status" => "fail", "error" => 4003, "message" => $this->error[4003]];
            $this->status_code = 400;
            return false;
        }
        return $new_balance;
    }

    private function _writeTransaction($type, $account_id, $amount, $balance)
    {
        $transaction             = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->type       = $type;
        $transaction->amount     = $amount;
        $transaction->balance    = $balance;
        $transaction->save();
        return $transaction->transaction_id;
    }

    private function _validate($data, $rules, $return = false)
    {
        $validated = Validator::make($data, $rules);

        if ($validated->fails()) {
            Log::info("Data validate fail");
            $this->return      = ["status" => "fail", "error" => 4001, "message" => $this->error[4001]];
            $this->status_code = 400;
            return false;
        }
        return true;
    }
}
