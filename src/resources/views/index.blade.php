<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<style>
		html,body{ margin: 0; padding: 0; }
		.container{
			margin-top: 20px;
			margin-bottom: 20px; 
		}
		.result{ margin: 20px 0; }
	</style>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					Test
				</div>
			</div>
			<div class="col-md-12">
				<form action="/api/v1" method="POST" role="form">
					<legend>Action</legend>
				
					<div class="form-group">
						<select name="action" class="form-control">
							<option value="create">Create account</option>
							<option value="delete">Delete account</option>
							<option value="balance">Show balance</option>
							<option value="deposit">Deposit</option>
							<option value="withdraw">Withdraw</option>
							<option value="transfer">Transfer</option>
						</select>
					</div>

					<div id="customer" class="form-group">
						<label for="">Customer id</label>
						<input type="number" class="form-control" name="customer_id" value="1" placeholder="">
					</div>


					<div id="type" class="form-group">
						<select name="type" class="form-control">
							<option value="SAVING">SAVING</option>
							<option value="CHEQUE">CHEQUE</option>
						</select>
					</div>

					<div id="account_1" class="form-group hide">
						<label for="">Account id</label>
						<input type="number" class="form-control" name="account_1" value="1" placeholder="">
					</div>
				
					<div id="amount" class="form-group hide">
						<label for="">Amount</label>
						<input type="number" class="form-control" name="amount" value="100" placeholder="">
					</div>
				
					<div id="account_2" class="form-group hide">
						<label for="">Account(2)</label>
						<input type="number" class="form-control" name="account_2" value="2" placeholder="">
					</div>

					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>

			<div class="col-md-12">
				<div class="result">
					<textarea cols="30" rows="3" class="form-control" placeholder="Response result"></textarea>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(function(){
			$("select[name=action]").change(function (e) {
				
				if($(this).val()=="create"){
					$("#customer, #type").removeClass('hide');
					$("#account_1,#account_2, #amount").addClass('hide');
				}
				else if($(this).val()=="deposit" || $(this).val()=="withdraw"){
					$("#account_1, #amount").removeClass('hide');
					$("#customer, #type").addClass('hide');
				}
				else if($(this).val()=="transfer"){
					$("#account_1, #account_2, #amount").removeClass('hide');
					$("#customer, #type").addClass('hide');
				}
				else if($(this).val()=="balance"){
					$("#account_1").removeClass('hide');
					$("#customer, #type, #account_2, #amount").addClass('hide');
				}
				else if($(this).val()=="delete"){
					$("#account_1").removeClass('hide');
					$("#customer, #type, #account_2, #amount").addClass('hide');
				}
				else{
					$("#account_2").addClass('hide');
				}
				return false;
			});
			$("form").submit(function(){
				var url = $("form").attr('action');
				var data = null;
				var method = 'GET';
				// console.log(data);
				// 
				switch($("[name=action]").val()){
					case "create":
						url +='/accounts';
						method = 'POST';
						data = {'customer_id':$("[name=customer_id]").val(),'type':$("[name=type]").val()};
						break;
					case "balance":
						url +='/accounts/'+$("[name=account_1]").val();
						break;
					case "deposit":
						url +='/accounts/'+$("[name=account_1]").val()+'/deposit';
						method = 'POST';
						data = {'amount':$("[name=amount]").val()};
						break;
					case "withdraw":
						url +='/accounts/'+$("[name=account_1]").val()+'/withdraw';
						method = 'POST';
						data = {'amount':$("[name=amount]").val()};
						break;
					case "transfer":
						url +='/accounts/transfer';
						method = 'PUT';
						data = {'from_account_id': $("[name=account_1]").val(),'to_account_id': $("[name=account_2]").val(),'amount':$("[name=amount]").val()};
						break;
					case "delete":
						url +='/accounts';
						method = 'DELETE';
						data = {'account_id':$("[name=account_1]").val()};
						break;
					default:
						return false;
						break;
				}

				$.ajax({
					url: url,
					type: method,
					contentType: 'application/json',
					data: JSON.stringify(data),
					success: function(res){
						$(".result").find('textarea').val(JSON.stringify(res));
					},
					error: function(xhr,status,err){
						console.log(xhr);
						$(".result").find('textarea').val(JSON.stringify(xhr.responseJSON));
					}
				});
				return false;
			});
		});
	</script>
</body>
</html>