<?php
/**
 * error code
 */
return [

	4001 => 'Missed paramters',
	4002 => 'Account not found',
	4003 => 'Account balance not enough',
	4004 => 'Transfer quota exceed',
	4005 => 'Account still have balance'

];